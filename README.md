# Hackathon – Spring Boot REST API
## Objective
The objectives for this hackathon are:
1. To work as a team to create a spring boot REST API that will become one component of a
financial trading platform.

2. To use git and bitbucket to share your code within the team and with your instructors

## Deliverable

You are tasked with creating one or more spring-boot REST API(s) to manage a portfolio of financial instruments (e.g. stocks). At the simplest level, this could consist of a single REST API which will manage “Trade” resources. In your first version a “Trade” might have the following attributes:

- Date created
- Stock Ticker
- Stock Quantity
- Requested Price
- Trade Status (E.g. all trades might initially be marked as “Created”, another component of the system may then “execute” pending trades and update the database record)

The initial version of your REST API should aim to facilitate CRUD operations through a RESTful HTTP interface (I.e. through a spring boot REST controller).

Consider some of the following concerns in your work:

- You should try to work as a team, this may mean “pair” or “mob” style programming. A large
part of this exercise is to demonstrate your ability to effectively self-organise and communicate as a team.
- You should create a bitbucket repository for your code giving all team members and your
instructor write access.
- You may consider using Trello, miro or some other simple task management system to plan
out your tasks and monitor your progress (e.g. a KANBAN board)
- If you are comfortable with git branching, you should try to use a sensible branching system e.g. create a dev branch and create feature branches off it, try to use PRs to merge branches.
- For your REST API, try to focus on code quality rather than quantity.
- For your REST API try to make use of unit testing. For example, for your service and entity classes you should be able to create unit-tests. For your REST/Controller layer, you could take a look at the unit tests in this project: https://github.com/nicktodd/springcourse/tree/master/Solutions/workspace/CompactDiscDaoWithRestAndBoot

## Trade State Extension

For this hackathon, you should aim to have a very simple version of the API functioning e.g. basic Create and Retrieve operations

If you have time you may consider the following extension:

If this component were included in a system that would update attempt to execute Pending trades, then Trade status may have one of a number of values, you may consider creating an Enum for these states (this is optional):
- CREATED
- PENDING
- CANCELLED
- REJECTED
- FILLED
- PARTIALLY FILLED
- ERROR

If your system includes such a set of possible Trade states, then you might consider implementing the following business rules:
- A CREATED Trade could be updated.
- A CREATED Trade could be updated to CANCELLED.
- A Trade in any other state can not be altered as it is now an official record.

## Further Extensions

If you have time you may also consider the following extension:

- A second REST API that will form another component of the trading system to keep track of the current holding portfolio. The records that this API manages would be updated by another component in the system as new trades are made and also periodically based on current market price (e.g. if a buy trade is executed for Google, then the current holding (quantity) for Google will be updated/created)
- Stock
- Current Holding (Quantity)
- Current Market Value

- You may consider how your Trades API could validate Stock Tickers against an external service e.g. Yahoo Finance. I.e. if a request is made to create a Trade for ticker “dfsl” your service would identify that this is not a valid ticker and so return the correct HTTP response code.
- This is a little more advanced, however you may research how you could create a “Scheduled Task” with spring boot, so that a new spring boot application could update the portfolio market value at regular intervals by reading from an Price service like Yahoo Finance. What concerns would there be around such a component in a “horizontally scaling” enterprise system?