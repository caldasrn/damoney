package com.conygre.spring.damoney.entities;

import java.util.Collection;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class User {

    @Id
    private ObjectId _id;
    private String username;
    private Collection <Trade> Portfolio;
    private double balance;

    public String get_id() {
        return _id.toHexString();
    }

    public void set_id(ObjectId _id) {
        this._id = _id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Collection<Trade> getPortfolio() {
        return Portfolio;
    }

    public void setPortfolio(Collection<Trade> portfolio) {
        Portfolio = portfolio;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

}