package com.conygre.spring.damoney.entities;

public enum TradeState {
    CREATED("CREATED"),
    PROCESSING("PROCESSING"),
    FILLED("FILLED"),
    REJECTED("REJECTED");

    private String tradeStatus;

    private TradeState(String state) {
        this.tradeStatus = state;
    }

    public String getState() {
        return this.tradeStatus;
    } 
}
