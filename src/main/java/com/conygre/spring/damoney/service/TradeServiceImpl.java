package com.conygre.spring.damoney.service;

import java.util.Collection;
import java.util.Optional;

import com.conygre.spring.damoney.entities.Trade;
import com.conygre.spring.damoney.repo.TradeRepository;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TradeServiceImpl implements TradeService {

    @Autowired
    private TradeRepository repo;

    @Override
    public void addTrade(Trade trade) {
        repo.insert(trade);
    }

    @Override
    public Collection<Trade> getTrades() {
        return repo.findAll();
    }

    @Override
    public Optional<Trade> getTradeById(ObjectId id) {
        return repo.findById(id);
    }

    @Override
    public void deleteTrade(ObjectId id) {
        repo.deleteById(id);
    }

    @Override
    public void updateTrade(ObjectId id, Trade trade) {
        trade.set_id(id);
        repo.save(trade);
    }
}
