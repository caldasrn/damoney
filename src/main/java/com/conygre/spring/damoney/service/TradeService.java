package com.conygre.spring.damoney.service;

import java.util.Collection;
import java.util.Optional;

import com.conygre.spring.damoney.entities.Trade;

import org.bson.types.ObjectId;

public interface TradeService {

    Collection <Trade> getTrades();

    Optional <Trade> getTradeById(ObjectId id);
    
    void addTrade(Trade trade);

    void deleteTrade(ObjectId id);

    void updateTrade(ObjectId id, Trade trade);
}
