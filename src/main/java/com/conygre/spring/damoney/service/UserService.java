package com.conygre.spring.damoney.service;

import java.util.Collection;
import java.util.Optional;

import com.conygre.spring.damoney.entities.User;

import org.bson.types.ObjectId;

public interface UserService {

    Collection <User> getUsers();
    Optional <User> getUserById(ObjectId id);
    void addUser(User user);
    void deleteUser(ObjectId id);
    void updateUser(ObjectId id, User user);
}