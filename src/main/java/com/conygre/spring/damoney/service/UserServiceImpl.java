package com.conygre.spring.damoney.service;

import java.util.Collection;
import java.util.Optional;

import com.conygre.spring.damoney.entities.User;
import com.conygre.spring.damoney.repo.UserRepository;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository repo;

    @Override
    public void addUser(User user) {
        repo.insert(user);
    }

    @Override
    public Collection<User> getUsers() {
        return repo.findAll();
    }

    @Override
    public Optional<User> getUserById(ObjectId id) {
        return repo.findById(id);
    }

    @Override
    public void deleteUser(ObjectId id) {
        repo.deleteById(id);
    }

    @Override
    public void updateUser(ObjectId id, User User) {
        User.set_id(id);
        repo.save(User);
    }
    
}