package com.conygre.spring.damoney.controllers;

import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;
import java.util.Optional;

import com.conygre.spring.damoney.entities.Trade;
import com.conygre.spring.damoney.service.TradeService;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@RestController
@RequestMapping ("/daMoney")
@CrossOrigin
public class TradeController {
    @Autowired
    private TradeService service;

    @RequestMapping(method = RequestMethod.GET)
    public Collection <Trade> getTrade(){ 
        return service.getTrades();
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{id}")
	public Optional<Trade> getTradeById(@PathVariable("id") String id) {
		return service.getTradeById(new ObjectId(id));
    }
    
    @RequestMapping(method = RequestMethod.POST)
    public void addTrade(@RequestBody Trade trade){
        service.addTrade(trade);
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/{id}")
    public void deleteTrade(@PathVariable("id") String id) {
        service.deleteTrade(new ObjectId(""+id));
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/{id}")
    public void updateTrade(@PathVariable("id") ObjectId id, @RequestBody Trade trade) {
        service.updateTrade(id, trade);   
    }
}
