/*
package com.conygre.spring.damoney.controllers;

import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;

import com.conygre.spring.damoney.entities.User;
import com.conygre.spring.damoney.service.UserService;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@RestController
@RequestMapping ("/daMoney")
@CrossOrigin
public class UserController {
    @Autowired
    private UserService service;

    @RequestMapping(method = RequestMethod.GET)
    public Collection <User> getUser(){ 
        return service.getUsers();
    }

    @RequestMapping(method = RequestMethod.POST)
    public void addUser(@RequestBody User user){
        service.addUser(user);
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/{id}")
    public void deleteUser(@PathVariable("id") String id) {
        service.deleteUser(new ObjectId(""+id));
    }


    @RequestMapping(method = RequestMethod.PUT, value = "/{id}")
    public void updateUser(@PathVariable("id") ObjectId id, @RequestBody User user) {
        service.updateUser(id, user);   
    }



}
*/