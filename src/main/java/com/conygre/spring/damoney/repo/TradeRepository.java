package com.conygre.spring.damoney.repo;

import com.conygre.spring.damoney.entities.Trade;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;


public interface TradeRepository extends MongoRepository<Trade, ObjectId> {
    
}