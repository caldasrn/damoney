package com.conygre.spring.damoney.repo;

import com.conygre.spring.damoney.entities.User;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;


public interface UserRepository extends MongoRepository<User, ObjectId> {
    
}